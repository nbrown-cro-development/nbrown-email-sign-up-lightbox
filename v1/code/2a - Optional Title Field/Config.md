# 1. Original Form Action Configuration
This file outlines all the settings within the Monetate action using the below format.
```bash
Option	|	Value
```
For example
```bash
How often (per user) should this lightbox be shown?	|	Every Page
```

## Required Inputs
```bash
HTML String											|	Copy & paste the contents of the `signup.html` file
Lightbox id (0-31 only)								|	10
How often (per user) should this lightbox be shown?	|	Once Per Session
Which submission method is preferred?				|	IFRAME
```
## Optional Inputs
```bash
Open lightbox on mouseout. 							|	Yes
Optional Thank You HTML Creative 					|	Copy & paste the contents of the `confirmation.html` file
Lightbox Class 										|	*Leave Blank*
CSS Styles 											|	Copy & paste the contents of the `variation.css` file
Top Position 										|	*Leave Blank*
Left Position 										|	*Leave Blank*
Pin Lightbox Position 								|	No
Overlay Color 										|	*Leave Blank*
Overlay Opacity 									|	*Leave Blank*
Closeable 											|	Yes
Refresh After? 										|	No
Responsive clickzones 								|	Yes
Responsive? 										|	No
Specific error for not valid email input 			|	Invalid email address
Generic error for required inputs 					|	*Leave Blank*
```
## Conditions
This is currently left blank but is subject to change on a test-by-test basis or at the global level.