/* Import tracking config for suppressing `console.log()` (using the trackingConfig.isTesting property) */
import trackingConfig from "..\\testConfig\\trackingConfig.js"
/* Import form validation rules from the `formValidation` module */
import formValidation from ".\\formValidation.js"
/* Import the `gaSendEvent` function from the `/genericFunctions/gaSendEvent` module */
import gaSendEvent from "..\\genericFunctions\\gaSendEvent.js"

/* Function to add/remove `invalid_field` class from fields and send event to GA */
export function fieldValidationHandler(field, valid = false) {
	if (valid) {
		gaSendEvent(`Form Field Completed: ${field.getAttribute('name')}`, "Email Sign Up Overlay")
		field.parentNode.classList.remove('invalid_field')
	} else {
		field.parentNode.classList.add('invalid_field')
		gaSendEvent(`Form Field Validation Error: ${field.getAttribute('name')}`, "Email Sign Up Overlay")
	}
}

/* function to handle a successful form submission, adds class to the overlay container */
export function formSubmitSuccessHandler() {
	let overlayElement = document.querySelector('.email-signup-overlay-container')
	overlayElement.classList.add('thank-you-visible')
}

/* function to handle a successful form submission, inserts a message above the submit CTA  */
export function formSubmitErrorHandler() {
	let tgt = document.querySelector('.email-signup-overlay-container #eMeSignUpBtn')
	tgt.insertAdjacentHTML('beforebegin', `<p class="formSubmitErrorMessage">Error submitting details, please try again later.</p>`)
}

/* function to handle the form submit event */
export function formSubmitHandler(e) {
	/* Prevent the form from submitting before we can run validaiton */
	e.preventDefault();
	e.stopPropagation();

	gaSendEvent("Form Submit Button Clicked", "Email Sign Up Overlay")

	/* Store all required field rows in a variable */
	let form = document.querySelector('.email-signup-overlay-container form')
	let requiredRows = form.querySelectorAll('[data-required]')
	/* Empty array to store validation results for each required field */
	let allRequiredFieldsValid = []
	/* Loop through the fields */
	requiredRows.forEach(row => {
		let field = row.querySelectorAll('input, select')[0]
		let name = field.getAttribute('name')
		let valid = false
		/* Check the field name attribute and apply the relevant validation rule to it */
		switch (name) {
			case 'Title':
			case 'FirstName':
			case 'Surname':
				valid = formValidation.textFieldValidationRules(field)
				break;
			case 'EmailAddress':
				valid = formValidation.emailFieldValidationRules(field)
				break;
			case 'EmailAddressConfirm':
				valid = formValidation.matchingEmailFields(field)
				break;
		}
		/* Push the validation result into the array */
		allRequiredFieldsValid.push(valid)
		/* Call `fieldValidationHandler()` to add/remove the validation messaging */
		fieldValidationHandler(field, valid)
	})

	/* Loop through all fields and send event to GA if field is empty. (Ignores MobilePhone field) */
	let fields = form.querySelectorAll('input, select')
	fields.forEach(field => {
		let name = field.getAttribute('name')
		if (name !== "MobilePhone") {
			let state = field.value !== "" ? "Completed" : "Empty"
			if (state === "Empty") {
				gaSendEvent(`Form Submitted: ${name}: Empty`, "Email Sign Up Overlay")
			}
		}
	})

	/* Check if `allRequiredFieldsValid` array contains a false value */
	if (allRequiredFieldsValid.indexOf(false) === -1) {
		/* If all required fields pass validation, send the form input to the API via AJAX */
		if (!trackingConfig.isTesting) {
			console.log('Submitting Email Sign Up Form')
		}
		$.ajax({
			url: form.getAttribute('action'),
			type: 'GET',
            data: $(form).serialize(),
		})
		.done(function(e) {
			/* Form submitted successfully, call `formSubmitSuccessHandler()` func and send event to GA */
			if (!trackingConfig.isTesting) {
				console.log('Successfully Submitted Email Sign Up Form')
			}
			formSubmitSuccessHandler()
			gaSendEvent("Form Submitted Successfully", "Email Sign Up Overlay")
		})
		.fail(function(e) {
			/* Form failed to submit, call `formSubmitErrorHandler()` func and send event to GA */
			console.error("Error Submitting Email Sign Up Form: ", e);
			formSubmitErrorHandler()
			gaSendEvent("Form Submission Error", "Email Sign Up Overlay")
		});
	}
}

/* Function to apply events to form fields and CTA */
export default function setFormHandlers() {
	let form = document.querySelector('.email-signup-overlay-container form')
	let requiredRows = form.querySelectorAll('[data-required]')
	/* Loop through all the required rows, adding the relevant validation rules to it */
	requiredRows.forEach(row => {
		let field = row.querySelectorAll('input, select')[0]
		let name = field.getAttribute('name')
		let fieldValidationCheck = _ => {
			return false;
		}
		switch (name) {
			case 'Title':
			case 'FirstName':
			case 'Surname':
				fieldValidationCheck = formValidation.textFieldValidationRules
				break;
			case 'EmailAddress':
			case 'EmailAddressConfirm':
				fieldValidationCheck = formValidation.emailFieldValidationRules
				break;
		}

        let eventListenerCB = e => {
            var valid = fieldValidationCheck(e.currentTarget);
            fieldValidationHandler(field, valid);
        }
        /* Attach the relevant field validation rules to the field with event listeners */
        field.addEventListener("change", eventListenerCB);
        // field.addEventListener("focusout", eventListenerCB);
	})
	/* Add the `formSubmitHandler()` func as a callback to for when the form is submitted  */
	form.addEventListener('submit', formSubmitHandler)
}