/* Tracking Config Settings */
const trackingConfig = {
	"isTesting"			: false,
	"customDimension"	: 18,
	"testID"			: "1001",
	"trackingID"		: "UA-76601428-1",
	"eventCategory"		: "CRO Test Data",
	"eventActionDetail"	: "ExampleTestName",
/* DO NOT EDIT BELOW THIS LINE ------------------------------ */
}

export default trackingConfig