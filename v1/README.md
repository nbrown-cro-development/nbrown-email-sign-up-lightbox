# V1 notes
This is the first iteration of the exit intent email sign up variations. It uses the prebuilt Monetate actions that we found to **not be fit for our purpose.**

**This version has been retained for future reference but is deprecated. Please use the V2 files.**

This version contains multiple variations of an exit intent triggered lightbox for driving email sign ups.
Each variation features differences to the form featured on the lightbox. These differences are listed below.

1. Original form
2. Optional Fields
	1. Optional title field
	2. Optional first name field
	3. Optional surname field
	4. Optional all non-email fields
3. Removed Fields
	1. Removed title field
	2. Removed first name field
	3. Removed surname field
	4. Removed all non-email fields
4. General Lightbox

## Folder Structure
```bash
. << Version >>						# Version sequence directly correlates with iteration number. i.e v1 = first iteration.
├── << Variation Name >>			# Name changes depending on variant. Reference list above.
|		├── Code					# Folder contains all the code.
|		|	├── signup.html 		# HTML file containing the intial overlay HTML.
|		|	├── confirmation.html 	# HTML file containing the confirmation overlay HTML.
|		|	└── varition.css 		# CSS file containing the styles for both overlays.
|		└── Config.md 				# Config file outlining the Monetate action options and what they should be set too.
```