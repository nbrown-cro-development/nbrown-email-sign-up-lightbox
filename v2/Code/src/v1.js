/* Import trackingConfig object */
import trackingConfig from "..\\modules\\testConfig\\trackingConfig.js"
/* Import the overlay CSS file as a string and store it in the overlayCSS variable */
import overlayCSS from "..\\modules\\testConfig\\overlay.css";
/* import various modules - Details are in the comments of each module */
import addStylesToDOM from "..\\modules\\genericFunctions\\addStylesToDOM.js"
import onMouseLeave from "..\\modules\\genericFunctions\\onMouseLeave.js"
import pollFunction from "..\\modules\\genericFunctions\\pollFunction.js"
import cookieHandler from "..\\modules\\genericFunctions\\cookieFunctions.js"
import gaSendEvent from "..\\modules\\genericFunctions\\gaSendEvent.js"
import setFormHandlers from "..\\modules\\formFunctions\\signUpForm.js"
import overlay from "..\\modules\\overlayFunctions\\signUpOverlay.js"

/* Update the `eventActionDetail`, appending it with the current variant */
trackingConfig["eventActionDetail"] = `${trackingConfig["eventActionDetail"]}-Variation-1`

/* Function to initialise the variant and call other functions */
function init() {
	/* Check if the body has the class `email_overlay_test_loaded` to prevent code running more than once */
	if (!document.body.classList.contains(`email_overlay_test_loaded`)) {
		/* Add `email_overlay_test_loaded` to the body to prevent code running more than once */
		document.body.classList.add(`email_overlay_test_loaded`);
		/* Send impression event */
		gaSendEvent("Loaded", "Email Sign Up Overlay", true)

		/* Add the CSS (stored in the overlayCSS variable) to the DOM */
		addStylesToDOM(overlayCSS)
		/* Call the `addOverlayToPage()` function from the `/overlayFunctions/signUpOverlay` module */
		overlay.addOverlayToPage()
		/* Call the `setFormHandlers()` function from the `/formFunctions/signUpForm` module */
		setFormHandlers()
		/* Call the `setCloseButtonHandlers()` function from the `/overlayFunctions/signUpOverlay` module */
		overlay.setCloseButtonHandlers()
		/* Call the `onMouseLeave()` function from the `/genericFunctions/onMouseLeave` module and pass an anonymous function to it */
		onMouseLeave(_ => {
			/* Check if the `email_overlay_test` cookie is false (or doesn't exist) */
			if (cookieHandler.checkCookie("email_overlay_test") === false) {
				/* Call the `overlayHandler()` function from the `/overlayFunctions/signUpOverlay` module and pass `true` to it to display the overlay */
				overlay.overlayHandler(true)
			}
		})
	}
}

/* This function declares the conditions for the poller */
function pollConditions() {
	/* Check if ga is defined - We don't want to run the variant if we can't track it */
	return typeof ga !== "undefined" &&
		/* Check if jQuery is defined - We're submitting the form via AJAX. If jQuery isn't defined then we won't be able to pass the users details to the API */
		typeof $ !== "undefined" &&
		/* Check if the body exists in the DOM - This should always be true but on the off-chance it isn't, we definitely don't want to run the code */
		document.querySelector('body') !== null &&
		/* Check if the `.email-signup-overlay-container` element exists - To prevent the code running more than once */
		document.querySelector('.email-signup-overlay-container') === null &&
		/* Check if the `email_overlay_test` cookie doens't exist - This is to prevent a user seeing the overlay more than once. (side note, Monetate should handle this for us but just in case it doesn't, we're performing this check) */
		cookieHandler.checkCookie("email_overlay_test") === false
}

/* Call the `pollFunction()` from the "/genericFunctions/pollFunction" module and pass the `pollConditions` and `init` functions to it. */
pollFunction(pollConditions, init)