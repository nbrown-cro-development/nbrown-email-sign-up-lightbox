# Exit Intent Lightbox - Email Sign Up
This experience contains multiple variations of an exit intent triggered lightbox for driving email sign ups.
Each variation features differences to the form featured on the lightbox. These differences are listed below.

1. Original form
2. Optional Fields
	1. Optional title field
	2. Optional first name field
	3. Optional surname field
	4. Optional all non-email fields
3. Removed Fields
	1. Removed title field
	2. Removed first name field
	3. Removed surname field
	4. Removed all non-email fields
4. General Lightbox

## Template Link
Desktop Link - https://marketer.monetate.net/control/a-c450c8bc/p/ambrosewilson.com/experience/1178492

## Action Config
The action features various settings, most of them should be left to the default unless specified in the Config.md file found in the variations folders.