import trackingConfig from "..\\testConfig\\trackingConfig.js"

/* Function to send events to GA. Accepts eventAction and eventLabel as strings and impressionEvent as a boolean (true/false) */
export default function gaSendEvent(eventAction = false, eventLabel = false, impressionEvent = false) {
	if (eventAction !== "" && eventLabel !== "") {
		let trackerName = `${trackingConfig.testID}_cro_tracker`
		let trackerExists = !!ga.getByName(trackerName) || false

		if (!trackerExists) {
			ga("create", trackingConfig.trackingID, {
				name: trackerName
			})
		}

		let eventObject = {
			'hitType': "event",
			'eventCategory': "CRO Test Data",
			'eventAction': `${trackingConfig.eventActionDetail}: ${eventAction}`,
			'eventLabel': eventLabel,
			'eventValue': '1',
			'nonInteraction': 1
		}

		if (impressionEvent !== false) {
			eventObject[`dimension${trackingConfig.customDimension}`] = `${trackingConfig.eventActionDetail}: ${eventAction}`
		}
		
		if (!trackingConfig.isTesting) {
			console.log(eventObject);
		}

		ga(`${trackerName}.send`, eventObject)
	}
}