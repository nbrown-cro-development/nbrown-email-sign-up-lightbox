# V2 notes
This version of the test will not make use of pre-built Monetate actions.

---

## How-To
The `./Code/src/` folder contains the individual variation files. Currently, this is just `control.js` and `v1.js`.
The `./Code/modules/` folder contains the individual modules that provide the logic for the variants.

#### Updating tracking details 
Tracking details are stored in the `trackingConfig` JS object. You can find this object in the `./Code/modules/testConfig/trackingConfig.js` file

#### Updating Overlay HTML & CSS
To update the HTML or CSS for the test, go to the `./Code/modules/testConfig/` folder and edit the `overlay.html` and/or `overlay.css` files respectively.

#### Configuring Required Form Fields
Form fields are designated as required by adding the `data-required` attribute to the parent div of the relevant `input`/`select` element. For the sake of clarity, the specific element also has the `form-row` class. To update this, please see the "Updating Overlay HTML & CSS" section above

#### Sending Additional Events to GA
Before adding an additional event, please consult the Events section below to determine if we are already tracking it or not.
To send additional events to GA, you can use the `gaSendEvent()` function. This function is stored in the `./Code/modules/genericFunctions/gaSendEvent.js` file. Please ensure this module is imported into the file where you are adding the additional `gaSendEvent()` function call.

---

### Events
The following is a list of all events fired within the test.
The first bullet point is a top-line description of the event name and the sub-bullet point is the event action that is sent to GA. Please note that the `ExampleTestName-Variation` will match the `eventActionDetail` in the `trackingConfig` module

* Variation Impression
	- `ExampleTestName-Variation: Loaded`
* Clicked Close Button
	- `ExampleTestName-Variation: Close Button Clicked`
* Overlay Hidden For User
	- `ExampleTestName-Variation: Overlay Hidden`
* Overlay Displayed to User
	- `ExampleTestName-Variation: Overlay Displayed`
* Clicked Dimmed Background
	- `ExampleTestName-Variation: Overlay Background Clicked`
* Form Field Validation Error
	- `ExampleTestName-Variation: Form Field Validation Error: <fieldName>`
* Form Field Empty on Submission
	- `ExampleTestName-Variation: Form Submitted: <fieldName>: Empty`
* Clicked Submit Button
	- `ExampleTestName-Variation: "Form Submit Button Clicked"`
* Form Field Completed
	- `ExampleTestName-Variation: Form Field Completed: <fieldName>`
* Form Submitted Successfully
	- `ExampleTestName-Variation: Form Submitted Successfully`
* Form Submission Error
	- `ExampleTestName-Variation: Form Submission Error`
* Clicked Thank You Button
	- `ExampleTestName-Variation: Thank You CTA Clicked`

---

### Folder Structure
```bash
. V2							# Version sequence directly correlates with iteration number. i.e v1 = first iteration.
└───Code						# Folder containing all the code files
    ├───modules					# The modules folder contains the individual modules for the variation  
    │   ├───formFunctions		# This folder contains the form handler functions for form submission, error handling and validation checking/handling
    │   ├───genericFunctions	# This folder contains misc. function files such as "addStylesToDOM" and relevant cookie functions (get, set, check)
    │   ├───overlayFunctions	# This contains overlay specific functionality such as adding the overlay to the page and hiding/displaying logic
    │   └───testConfig			# This folder contains the tracking config details, and the overlay HTML and CSS files
    └───src 					# This contains the v1 JS file that utlises the modules in the /modules/ directory
```