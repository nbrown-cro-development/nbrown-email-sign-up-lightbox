/* eslint-disable */
const { watch, src, dest, series, parallel } = require("gulp"),
    autoprefixer = require('autoprefixer'),
    babel = require("gulp-babel"),
    rollup = require("gulp-better-rollup"),
    cleanup = require("rollup-plugin-cleanup"),
    htmlRollup = require("rollup-plugin-html"),
    cssRollup = require("@atomico/rollup-plugin-import-css"),
    del = require("del"),
    prettier = require("gulp-prettier"),
    terser = require("gulp-terser"),
    rename = require("gulp-rename");

const src_folder = "src/",
    prod_folder = "prod/",
    modules_folder = "modules/";

const settings = {
    clean: true,
};

const paths = {
    src: src_folder,
    prod: prod_folder,
    modules: modules_folder,

    js: {
        src: `${src_folder}`,
        prod: `${prod_folder}`,
    }
};

/**
 * Process Javascript Files
 */
function javascript() {
    return src(`${paths.js.src}*.js`)
        .pipe(
            rollup(
                {
                    plugins: [
                        htmlRollup({
                            include: `${paths.modules}*/*.html`,
                            htmlMinifierOptions: {
                                collapseWhitespace: true,
                                conservativeCollapse: true
                            }
                        }),
                        cssRollup({
                            include: `${paths.modules}*/*.css`,
                            minimize: true,
                            plugins: [autoprefixer()]
                        }),
                        cleanup({
                            maxEmptyLines: -1,
                            compactComments: false,
                            comments: "all",
                            sourcemap: false,
                        })
                    ],
                },
                {
                    format: "iife",
                }
            )
        )
        .pipe(babel())
        .pipe(prettier())
        .pipe(dest(paths.js.prod))
        .pipe(rename({ suffix: ".min" }))
        .pipe(terser())
        .pipe(dest(paths.js.prod));
}

/**
 * Watch src files
 */
function watchSrc(done) {
    watch([paths.src, paths.modules], series(exports.default));
    done();
}

/**
 * Clear all files from prod only
 */
function clearProd(done) {
    if (!settings.clean) return done();

    del.sync([paths.prod]);

    return done();
}
exports.clearProd = clearProd;

/**
 * Default scripts
 */
exports.default = series(clearProd, parallel(javascript));

/**
 * Watch Script
 */
exports.watch = series(exports.default, watchSrc);