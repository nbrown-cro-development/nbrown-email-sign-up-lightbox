/* Text Field Validation - Checks that the field is not an empty string */
export function textFieldValidationRules(field) {
	return field.value.replace(/ /g, '') !== ""
}

/* Email Field Validation - Checks that the field is not an empty string and matches the below email address regex */
export function emailFieldValidationRules(field) {
	return field.value.replace(/ /g, '') !== "" && /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(field.value)
}

/* Email Field Matching - Checks both email fields are not empty and that the values match (without leading/trailing whitespace and is not case sensitive) */
export function matchingEmailFields() {
	let emailFieldA = document.querySelector('.mt_email-lightbox__form input#mtEmail')
	let emailFieldB = document.querySelector('.mt_email-lightbox__form input#mtEmailConfirm')
	return (emailFieldA.value.trim().toLowerCase() === emailFieldB.value.trim().toLowerCase()) && emailFieldValidationRules(emailFieldA) && emailFieldValidationRules(emailFieldB)
}

/* Group functions together in `formValidation` for exporting the module */
const formValidation = {
	"matchingEmailFields"		: matchingEmailFields,
	"textFieldValidationRules"	: textFieldValidationRules,
	"emailFieldValidationRules"	: emailFieldValidationRules
}

export default formValidation