/* Import overlay HTML from the `/testConfig/overlay.html` file and store as a string in a variable */
import overlayHTML from "..\\testConfig\\overlay.html";
/* Import cookie and gaSendEvent functions from relevant modules */
import cookieHandler from "..\\genericFunctions\\cookieFunctions.js";
import gaSendEvent from "..\\genericFunctions\\gaSendEvent.js"

/* Function to handle hiding/showing the overlay via a body class and send relevant events to GA */
export function overlayHandler(showOverlay = false) {
	let body = document.querySelector('body')

	if (showOverlay) {
		body.classList.add('show_email_overlay')		
		cookieHandler.setCookie('email_overlay_test', true)
		gaSendEvent("Overlay Displayed", "Email Sign Up Overlay")
	} else {
		gaSendEvent("Overlay Hidden", "Email Sign Up Overlay")
		body.classList.remove('show_email_overlay')		
	}
}

/* Function to call `overlayHandler()` func to hide overlay */
export function overlayCloseButtonHandler() {
	overlayHandler(false)
}

/* Function to insert the overlay HTML to the DOM as the last element in the body */
export function addOverlayToPage() {
	let tgt = document.querySelector('body')
	tgt.insertAdjacentHTML('beforeend', overlayHTML)
}

/* Function to apply relevant click listeners to close the overlay */
export function setCloseButtonHandlers() {
	/* Add listener to the close button */
	document.querySelectorAll('.email-signup-overlay-container .email-signup-overlay-close').forEach(btn => {
		btn.addEventListener('click', e => {
			e.preventDefault();
			e.stopPropagation();
			gaSendEvent("Close Button Clicked", "Email Sign Up Overlay")
			overlayCloseButtonHandler()
		})
	})
	/* Add listener to the thank you CTA */
	document.querySelectorAll('.email-signup-overlay-container .thank-you a.btn.primaryBtn').forEach(btn => {
		btn.addEventListener('click', e => {
			e.preventDefault();
			e.stopPropagation();
			gaSendEvent("Thank You CTA Clicked", "Email Sign Up Overlay")
			overlayCloseButtonHandler()
		})
	})
	/* Add listener to the overlay background */
	document.querySelectorAll('.email-signup-overlay-container .email-signup-overlay-background').forEach(btn => {
		btn.addEventListener('click', e => {
			e.preventDefault();
			e.stopPropagation();
			gaSendEvent("Overlay Background Clicked", "Email Sign Up Overlay")
			overlayCloseButtonHandler()
		})
	})
}

/* Group functions together in `overlay` object for exporting the module */
const overlay = {
	overlayHandler: overlayHandler,
	overlayCloseButtonHandler: overlayCloseButtonHandler,
	addOverlayToPage: addOverlayToPage,
	setCloseButtonHandlers: setCloseButtonHandlers,
}

export default overlay